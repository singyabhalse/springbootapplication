package com.ldspl.person.web.rest.resource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.assertj.core.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ldspl.person.db.entities.Course;
import com.ldspl.person.db.entities.Person;
import com.ldspl.person.db.entities.Student;
import com.ldspl.person.db.entities.StudentCourse;
import com.ldspl.person.db.entities.StudentDemographic;

@RestController
@RequestMapping(value = "/person")
public class PersonResource {

	public List<Person> sort(List<Person> people) {
		Comparator<Person> compareDOB = (Person o1, Person o2) -> o1.dob.compareTo(o2.dob);
		Collections.sort(people, compareDOB);
		System.out.println("--------------------------------------------------------------------------------");
		System.out.println("People List sorted by DOB ");
		people.forEach(person -> System.out.println(person.toString()));
		System.out.println("--------------------------------------------------------------------------------");
		return people;
	}

	public Person find(List<Person> people, Date dob) {
		return people.stream().filter(p -> p.dob.compareTo(dob) == 0).findAny().orElse(null);
	}

	public String maxState(List<Person> people) {
		return people.stream().collect(Collectors.groupingBy(Person::getState, Collectors.counting())).entrySet()
				.stream().max(Map.Entry.comparingByValue()).get().getKey();

	}

	@GetMapping("/sort")
	public Map<String, Object> getPersonList() throws ParseException {
		Map<String, Object> map = new HashMap<String, Object>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Person p1 = new Person(1, "Rohit", sdf.parse("1992-02-28"), "M.P.");
		Person p2 = new Person(2, "Virat", sdf.parse("1990-07-21"), "M.H.");
		Person p3 = new Person(3, "Jaspreet", sdf.parse("1994-08-21"), "M.H.");

		List<Person> people = new ArrayList<>();
		people.add(p1);
		people.add(p2);
		people.add(p3);
		// million such entries will get added

		List<Person>  peopleList=sort(people);
		Person person = find(people, sdf.parse("1992-02-28"));
		System.out.println("get person DOB => " + person.toString());
		System.out.println("-------------------------------------------------");
		String stateName = maxState(people);
		System.out.println("stateName =" + stateName);
		System.out.println("--------------------------------------------------------------------------------");
		
		map.put("People sorted List", peopleList);
		map.put("stateName", stateName);
		map.put("find people by DOB", person);
		// person.stream().collect(Collectors.groupingBy(Person::getState));
		return map;

	}

	@GetMapping("/student/{state}")
	public int student(@PathVariable String state) {
		int count = stateScore(state);
		System.out.println("Count=====> " + count);
		return count;
	}

	public int stateScore(String inputState) {
		List<Course> courses = new ArrayList<>();
		List<Student> students = new ArrayList<>();
		List<StudentDemographic> demographics = new ArrayList<>();
		List<StudentCourse> registrations = new ArrayList<>();

		Student student1 = new Student(1, "Jaspreet", 1);
		Student student2 = new Student(2, "Rohit", 2);
		Student student3 = new Student(3, "Virat", 2);
		students.add(student1);
		students.add(student2);
		students.add(student3);

		Course CS101 = new Course(1, "CS101");
		Course CS102 = new Course(2, "CS102");
		courses.add(CS101);
		courses.add(CS102);

		StudentDemographic studentDemographic1 = new StudentDemographic(1, 20, "Banglore", "Kerela");
		StudentDemographic studentDemographic2 = new StudentDemographic(2, 22, "Amaritsar", "Punjab");
		demographics.add(studentDemographic1);
		demographics.add(studentDemographic2);

		StudentCourse studentCourse1 = new StudentCourse(1, 1);
		StudentCourse studentCourse2 = new StudentCourse(1, 2);
		StudentCourse studentCourse3 = new StudentCourse(2, 2);
		registrations.add(studentCourse1);
		registrations.add(studentCourse2);
		registrations.add(studentCourse3);

		List<Long> stateIds = demographics.stream().filter(dmg -> dmg.getState().equalsIgnoreCase(inputState))
				.map(StudentDemographic::getId).collect(Collectors.toList());

		if (stateIds != null && !stateIds.isEmpty()) {

			List<Long> stuIds = students.stream().filter(st -> st.getDemographicId() == stateIds.get(0))
					.map(Student::getId).collect(Collectors.toList());

			int count = 0;
			for (Long sId : stuIds) {
				List<StudentCourse> stC = registrations.parallelStream().filter(st -> st.getStudentId() == sId)
						.collect(Collectors.toList());

				if (!stateIds.isEmpty()) {
					count = count + stC.size();
				}
			}
			return count;
		}
		return 0;
	}
}
