package com.ldspl.person.db.entities;

public class StudentCourse {
	public long studentId;
	public long courseId;

	public StudentCourse(long studentId, long courseId) {
		super();
		this.studentId = studentId;
		this.courseId = courseId;
	}

	public long getStudentId() {
		return studentId;
	}

	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}

	public long getCourseId() {
		return courseId;
	}

	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
}
