package com.ldspl.person.db.entities;

public class Student {
	public long id;
	public String name;
	public long demographicId;

	public Student(long id, String name, long demographicId) {
		super();
		this.id = id;
		this.name = name;
		this.demographicId = demographicId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getDemographicId() {
		return demographicId;
	}

	public void setDemographicId(long demographicId) {
		this.demographicId = demographicId;
	}
}
