package com.ldspl.person.db.entities;

public class StudentDemographic {
	public long id;
	public int studentAge;
	public String studentCity;
	public String state;

	public StudentDemographic(long id, int studentAge, String studentCity, String state) {
		super();
		this.id = id;
		this.studentAge = studentAge;
		this.studentCity = studentCity;
		this.state = state;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getStudentAge() {
		return studentAge;
	}

	public void setStudentAge(int studentAge) {
		this.studentAge = studentAge;
	}

	public String getStudentCity() {
		return studentCity;
	}

	public void setStudentCity(String studentCity) {
		this.studentCity = studentCity;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
